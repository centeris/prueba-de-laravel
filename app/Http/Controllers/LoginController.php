<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        if ($request->ajax()){
            $credentials = $request->only('email', 'pasword');

            if (Auth::attempt($credentials)) {
                return response()->json(route('dashboard'), 200);
            }

            return response()->json("Error", 406);
        }
    }
}
